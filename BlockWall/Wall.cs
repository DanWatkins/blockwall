﻿namespace BlockWall
{
    public class Wall
    {
        public Point TilePosition { get; set; }

        public Orientation Orientation { get; set; }
    }
}
