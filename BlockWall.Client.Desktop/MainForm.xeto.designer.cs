﻿using Eto.Forms;

namespace BlockWall.Client.Desktop
{
    public partial class MainForm
    {
        private StackLayout GameBoardStackLayout { get; set; }

        private Label OpponentWallsLabel { get; set; }

        private Label PlayerWallsLabel { get; set; }
    }
}